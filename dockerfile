FROM meteor/ubuntu:latest

# meteor installer doesn't work with the default tar binary
RUN apt-get update\
    && apt-get install -y bsdtar \
    && cp $(which tar) $(which tar)~ \
    && ln -sf $(which bsdtar) $(which tar)

# install Meteor forcing its progress
RUN curl https://install.meteor.com/ | sh

# put back the original tar
RUN mv $(which tar)~ $(which tar)

COPY [".meteor/", "package.json", "/usr/src/"]

WORKDIR /usr/src

RUN meteor npm install

COPY ["client/", "imports/", "public/", "server/", "tests/", "settings.json", "/usr/src/"]
EXPOSE 3000

CMD ["meteor", "--settings", "settings.json", "--allow-superuser"]
