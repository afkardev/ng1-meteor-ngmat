import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './afkarDocList.html';
import './afkarDocList.less';

class AfkarDocList {
  constructor($reactive, $scope) {
    $reactive(this).attach($scope);
    var imagePath = 'afkar-mini.png';

    this.docs = [
    {
      img : imagePath,
      title: 'Brunch this weekend?',
      author: 'Min Li Chan',
      message: " I'll be in your neighborhood doing errands"
    },
    {
      img : imagePath,
      title: 'Brunch this weekend?',
      author: 'Min Li Chan',
      message: " I'll be in your neighborhood doing errands"
    },
    {
      img : imagePath,
      title: 'Brunch this weekend?',
      author: 'Min Li Chan',
      message: " I'll be in your neighborhood doing errands"
    },
    {
      img : imagePath,
      title: 'Brunch this weekend?',
      author: 'Min Li Chan',
      message: " I'll be in your neighborhood doing errands"
    },
    {
      img : imagePath,
      title: 'Brunch this weekend?',
      author: 'Min Li Chan',
      message: " I'll be in your neighborhood doing errands"
    },
    ];
    this.helpers({
      afkar(){
      }

    })
  }
}

const name = 'afkarDocList';

export default angular.module(name, [
  angularMeteor
  ])
.component(name, {
  template: template,
  controller: ['$reactive', '$scope', AfkarDocList],
  controllerAs: "ctrl"
});
