import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from '@uirouter/angularjs';

import template from './home.html';
import './home.less';

import { name as DocList } from '/imports/ui/components/common/afkarDocList/afkarDocList';

class Home {
	constructor() {
	}
}

const name = 'home';

export default angular.module(name, [
	angularMeteor,
	uiRouter,
	DocList
	])
.component(name, {
	template: template,
	controller: Home,
	controllerAs: "ctrl"
})
.config(config);

function config($stateProvider) {
	'ngInject';
	$stateProvider
	.state(name, {
		url: '/afkar',
		template: '<home></home>'
	});
}
